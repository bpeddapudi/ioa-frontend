console.log("JS is connected");

(function() {

var angularModule = angular.module('mainApp', ['ui.router','ngFileUpload','elasticsearch']);

// configuring our routes 
// =============================================================================
angularModule.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('dashboard', {
            url: '/Dashboard',
            templateUrl: 'views/dashboard.html',
            controller: 'mainController'
        })
        .state('dashboard.results', {
            url: '/results',
            templateUrl: 'views/dash-results.html'
        })
        .state('dashboard.details', {
            url: '/details',
            templateUrl: 'views/details.html'
        })
        .state('start', {
            url: '/start',
            templateUrl: 'views/start.html',
            controller: 'mainController'
        });
        
    $urlRouterProvider.otherwise('/Dashboard');
});

angularModule.service('es', function (esFactory) {
      return esFactory({
        host: 'localhost:9200',
        apiVersion: '1.2',
        log: 'trace'
      });
    });
// our controller
// =============================================================================
angularModule.controller('mainController', ['$scope','$http','$state', 'Upload','$timeout',function( $scope,$http,$state,Upload,$timeout) {
 //$scope.elasticURL = "http://localhost:9200/companies/company";
  $.getJSON('data/sideMenuCategories.json', function(data) {
         $scope.sideMenuObjs= data;
         console.log("SideMenuobjess:"+JSON.stringify($scope.sideMenuObjs));
  });
  $scope.properties = {};
  $scope.elasticURL = "";
 $.getJSON('data/properties.json', function(data) {
         $scope.properties = data;
        // console.log("updated properties:"+JSON.stringify($scope.properties.elasticURL));
         $scope.elasticURL = $scope.properties.elasticURL;
         //console.log("elasticURL:"+$scope.elasticURL);
  });
    $scope.detailsObject = {};
    $scope.changeDetailsObj = function(){
        $scope.detailsObject.title = "Title has been chagned";
        $scope.detailsObject.description = "Congratulations description  has also been update now you can make any changes you want";
    };
    $scope.uploadFile = function(){
        var fileNameIndex = $("#fileUploadInput").val().lastIndexOf("\\") + 1;
        var filename =$("#fileUploadInput").val().substr(fileNameIndex);
                //        console.log("index:"+fileNameIndex);
                //        console.log("File name:"+filename);
        $http({
        method : "GET",
        url : $scope.properties.fileUploadURL+filename
        }).then(function mySucces(response) {
           console.log("response:"+response.data);
        }, function myError(response) {
            console.log("error:"+response.statusText); 
        });  
    };
    $scope.getFileter = function(){
        $.getJSON('data/filters.json', function(data) {
        for(var i=0; i< $scope.sideMenuObjs.length ; i++)
            {  $scope.sideMenuObjs[i].options = [];
               data.aggregations[$scope.sideMenuObjs[i].id].buckets;
               for(var j=0 ; j<data.aggregations[$scope.sideMenuObjs[i].id].buckets.length ; j++)
               {
                   $scope.sideMenuObjs[i].options.push(data.aggregations[$scope.sideMenuObjs[i].id].buckets[j].key);
                   //console.log("Filters:"+JSON.stringify(data.aggregations[$scope.sideMenuObjs[i].id].buckets[j].key));
               }
            }  
        
        });
    };
    $scope.getFileter();
    $scope.filtersActive = [
        {
            "id":"industry_vertical",
            "category":"Industry Vertical",
            "options":[]
        },
        {   "id":"industry_1",
            "category":"industry1",
            "options":[]
        },
        {   "id":"primary_plcd",
            "category":"Primary IOA Use Case",
            "options":[]
        },
        {
            "id":"secondary_plcd",
            "category":"Secondary IOA Use Case",
            "options":[]
        }
    ];
    $scope.resultList = [];
    $scope.filtersOnResults = [];
    $scope.getCustomers = function(){
        console.log("getCustomers function");
        var queryString =$scope.elasticURL+"/_search?fields=_id,customer,customer_description&q=";
        var filtersObj = [];
        //console.log("length of filters cat:"+ filtersObj.length);
        for(var i =0 ; i<$scope.filtersActive.length ;i++)
        {
            if($scope.filtersActive[i].options.length !== 0){
                filtersObj.push($scope.filtersActive[i]);
            }
        }
        console.log("filtersObj.length:"+filtersObj.length);
        for(var i =0 ; i<filtersObj.length ;i++)
        {
            console.log("filetersObj.options.length:"+filtersObj[i].options.length);
            queryString = queryString + "(";
            for(var j=0; j< filtersObj[i].options.length; j++)
             {
                
                  if(j === 0)
                    queryString = queryString +filtersObj[i].id+":\""+ filtersObj[i].options[j].replace("&","%26")+"\"";
                  else
                    queryString = queryString +" OR "+filtersObj[i].id+":\""+ filtersObj[i].options[j].replace("&","%26")+"\"";
                console.log("obj:"+filtersObj[i].options[j]) ;
             }
             queryString = queryString+")";
            if(i < filtersObj.length-1 )
            {
                queryString = queryString +" AND ";
            }      
        }
        queryString = queryString+"&size=9999&sort=customer";
        console.log("query:"+queryString);
        $http({
        method : "GET",
        //url : "http://localhost:9200/companies/company/_search?fields=_id,customer,customer_description&q=industry_vertical:(%22Manufacturing%22%20OR%20%22Health%20Care%22)%20AND%20primary_plcd:%22Locations%22"
        url : queryString
        }).then(function mySucces(response) {
           console.log("response:"+response.data);
           $scope.resultList=[];
           for(var i=0; i<response.data.hits.hits.length; i++){
               var customer = {
                   "id":"",
                   "name":"",
                   "description":""
               };
               customer.name = response.data.hits.hits[i].fields.customer[0];
               customer.description = response.data.hits.hits[i].fields.customer_description[0];
               customer.id = response.data.hits.hits[i]._id;
               $scope.resultList.push(customer);
           }
           
        }, function myError(response) {
            console.log("error:"+response.statusText); 
        });  
        $scope.filtersOnResults = filtersObj ;
    };
    $scope.customerIn = "No response";
    $scope.getCustomeInfo = function(Id){
        //console.log("retrive the customer details:"+Id);
        $http({
        method : "GET",
        url : $scope.elasticURL+"/_search?q=_id:"+Id
        }).then(function mySucces(response) {
           console.log("response:"+response.data);
           $scope.detailsObject = response.data.hits.hits[0]._source;
           $scope.customerIn = $scope.detailsObject["customer"];
           var values =[];
           for(var k in $scope.detailsObject) 
           {  
               if($scope.detailsObject[k]!== '')
                {
                    console.log("Empty key:"+k);
                    var temp = [k.replace(/_/g, " "), $scope.detailsObject[k] ];
                    values.push(temp);
                }
           }
           $scope.detailsObject = values.sort();
           
          // $scope.test = values;
           console.log("Keys:"+values[0]);
        }, function myError(response) {
            console.log("error:"+response.statusText); 
        });  
        
    };
    $scope.checkFiltersOn = function(keyIn,valueIn){
      //console.log("KeyIn:"+keyIn+"-valueIn:"+valueIn);
      for (var i = 0; i < $scope.filtersActive[keyIn].options.length; i++) {
            if ($scope.filtersActive[keyIn].options[i] === valueIn) {
              //  console.log("true:checkFiletrsOn");
                return i;
            }
       }
      return -1;
    };
    $scope.activateFiletr = function(categoryKeyIn,filterKeyIn){
       // console.log("categoryKeyIn, ");
       var validator =  $scope.checkFiltersOn(categoryKeyIn,$scope.sideMenuObjs[categoryKeyIn].options[filterKeyIn]);
       if(validator>=0)
         {
           //  console.log("true:activateFilter:"+validator); 
             $scope.filtersActive[categoryKeyIn].options.splice(validator,1);
         }
       else
         {
            // console.log("false:activateFilter");
             $scope.filtersActive[categoryKeyIn].options.push($scope.sideMenuObjs[categoryKeyIn].options[filterKeyIn]);
         }
    };
    
     $scope.responseFlag = {
            "type":"",
            "message":"",
            "class":"",
            "hidden":true
        };
        $scope.progressBar = {
           "prgressBarClass":"progress-bar progress-bar-info progress-bar-striped active",
           "hidden":true
        };
        console.log("Hidden:"+ $scope.progressBar.hidden);
      $scope.uploadFiles = function(file, errFiles) {
       
                $scope.progressBar.hidden = false;   
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            console.log("FIle:"+file);
            file.upload = Upload.upload({
                url: 'http://ch3lxcmwasd01:8090/upload',
                data: {filePath: file}
            });

            file.upload.then(function (response) {
                console.log("response:"+response.data);
                if(response.data.indexOf("Error") > -1)
                {    
                $scope.responseFlag.type = "Error:";
                $scope.responseFlag.message = response.data ; 
                $scope.responseFlag.class = "alert alert-danger fade in" ; 
                $scope.responseFlag.hidden = false;
                $scope.progressBar.prgressBarClass = "progress-bar progress-bar-danger progress-bar-striped active";
                }
                else
                {
                  $scope.responseFlag.type = "Sucess:";
                  if(response.data.indexOf("200")>-1)
                      $scope.responseFlag.message = "File uploaded successfully";
                  //$scope.responseFlag.message = response.data ; 
                  $scope.responseFlag.class = "alert-success fade in" ; 
                  $scope.responseFlag.hidden = false;
                  $scope.progressBar.hidden = false;
                  $scope.progressBar.prgressBarClass = "progress-bar progress-bar-success progress-bar-striped active";  
                }
                $timeout(function () {
                    file.result = response.data;
                });
            }, function (response) {
               console.log("Error responnse:"+response.status);
                $scope.responseFlag.type = "Error:";
                if(response.status)
                    $scope.responseFlag.message = "Connection Refused: backend connection is refused" ; 
                $scope.responseFlag.class = "alert alert-danger fade in" ; 
                $scope.responseFlag.hidden = false;
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * 
                                         evt.loaded / evt.total));
            });
        }   
    };

}])
;

})();
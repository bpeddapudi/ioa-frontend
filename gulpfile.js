/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var gulp = require('gulp');

//sass plugin
var sass = require('gulp-sass');
var useref = require('gulp-useref');
gulp.task('default', function () {
    console.log('Running into the default task');
});
gulp.task('test', function(){
    console.log('Testing task is running now');
});

gulp.task('sass',function(){
    return gulp.src('public_html/scss/style.scss')
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe(gulp.dest('public_html/css'));
    console.log('Sass to css transfer complete');
});
gulp.task('useref', function(){
  return gulp.src('public_html/*.html')
    .pipe(useref())
    .pipe(gulp.dest('public_html/js'));
});
gulp.task('watch', function(){
    gulp.watch('public_html/scss/**/*.scss',['sass']);
});